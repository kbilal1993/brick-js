# Why is it called Brick?
It's the first thing I could think of that wasn't taken. I like the idea of the library providing function "bricks" that you can piece together to make a website.
# What if I want to minify the code?
Minifying the library will improve loading times by a lot, but it also hides how the library works. I'd rather people have the ability to read through everything, but if you want to minify it there's nothing stopping you.
# Why did you pollute the global space with so many functions?
Because I want to type p("text"), not brick.p("text").
# Will this make my web page bloated?
People block clientside JavaScript because most web pages are so full of autoplaying videos, analytics code and unneccesary features that turning it all off improves the experience. This is the fault of the site designer, rather than JavaScript itself. You can use this library to make sleek, minimalist, functional web pages just fine. There shouldn't even be a performance penalty compared to using HTML and CSS. The only downside is that the client will be asked to enable JavaScript to view the page, and if they don't they'll just get an empty page.

By the way, HTML has a tag that only shows if a client has disabled JavaScript. You'll want to add this to your HTML:  
`<noscript>You need JavaScript to view this page</noscript>`
# TypeScript?
I'd add support if browsers natively supported it. For now you have to transpile it, and I want this library to be as accessible as possible. Besides, the original idea behind it was to exclusively use JavaScript.
# Can I use this in production?
Sure. Just remember that the library is still in development, and I might make breaking changes. I'll try to note them down somewhere if I do.
# Where would I not want to use this?
When you expect clients to not have JavaScript enabled. Tor browsers usually disable it by default, and text browsers mostly don't support it (I think elinks does). All they'll see is a few `<script>` tags.  
You also might have issues with SEO. Not sure how that works, but if search engines just read the source code for text then all they'll find is `<script>`.
# Why does the resulting page not go in the `<body>` tag?
I'm a little torn on using `<body>` personally. The problem is that the tag only appears once the page is loaded, but I don't want users to have to `window.onload = () => add(document.body, whatever)`.  
Currently we generate the page directly in `<html>`, and the body tag gets removed on page load (because Chrome-likes still display the body and it makes the web page longer than it needs to be). I'm not sure if this will lead to any incompatibilities down the line, and currently it seems to work just fine.
