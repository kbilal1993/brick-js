# Web Development and JavaScript's Reputation
Web development is an interesting case. It's built on multiple layers of obsolete standards and technologies, but the Web is so important that we can't just get rid of it all. That's why the `<marquee>` tag is still technically valid, and why JavaScript contains so many quirks. Some developers try to fix this by making their own frameworks, but that leads to the other big problem with Web dev; There are far too many frameworks and libraries to make life easier, and frameworks built on frameworks which become popular for a month or so until they get dumped for the next new thing.

Web development isn't sleek and efficient. It's tools are full of deprecated features, and when you read any tutorials everything you find will be equally crusty and obsolete. JavaScript especially gets a bad reputation on the Internet, and it's a rite of passage for new developers to claim that it's the worst thing since ActiveX. Personally I used to hold this opinion too, but after using JavaScript for a while I've started to warm up to it.

# Dabbling with dark magic
Websites are designed with three main tools. There's HTML, which defines the contents of the page, CSS, which defines the layout and style of the content, and JavaScript, which adds functionality. The cool thing is that everything you do in HTML and CSS can also be done in JavaScript. The structure of the website appears in the JavaScript variable `document`, in what they call the Document Object Model. This means that you could just make an entire web page using JavaScript alone.

Here's the theory: We make a HTML file which immediately loads a JavaScript file, and we put the entire logic of the web page in that one file. We then use JS DOM functions to create HTML nodes and place them in the right parts of the web page. In practice this works perfectly, but DOM nodes are a bit of a pain to work with so it ends up being more annoying. That's not going to stop me though.

My solution is to make a library of wrapper functions to make this easier. If I want a paragraph I don't want to define a `p` node and then add a text node containing my desired text, I want to type `p('hello')` and get a node equivalent to `<p>hello</p>`. And that's what I did. Read `library.js` if you want to see how it works.

# Basic setup
Copy template.html from the public folder. Create a file in the same directory named "main.js". Open template.html in your browser, main.js in your text editor of choice, and start developing. Refresh the browser to see any changes.

It helps to open your browser's console as you develop your web page. Any JS errors will show up there, and they are descriptive enough to help track down and fix any mistakes.

# Making a simple web page
Consider the following:  
`// Creating nodes`  
`var box = div();`  
`var title = h1("My web page")`  
`var inside_text = p("Welcome to my cool web page!");`  
`// Putting the nodes where they're supposed to be`  
`add(box, title);`  
`add(box, inside_text);`  
`add(root, box);`  

This will create the following HTML:  
`<div>`  
`  <h1>My web page</h1>`  
`  <p>Welcome to my cool web page!</p>`  
`</div>`  

We can style elements by interacting with their node variables:  
`// for any node, .style contains styling information`  
`title.style.backgroundColor = "green";`  

Look at example 2 to see more of this in action.

# Lists
We've got some extra functions to make list and table creation easier. Here's a taste:  
`var my_values = ["One", "Two", "Three"];`  
`var my_list = ol(my_values);`  

my_list becomes a node which represents the following:  
`<ol>`  
`  <li>One</li>`  
`  <li>Two</li>`  
`  <li>Three</li>`  
`</ol>`  

If you provide ol() with a list of DOM nodes, it uses them directly. If you use a list of strings, it automatically wraps them in text nodes and uses them. How convenient. ul() and table() follow the same rules.

# Separating components
When a HTML file loads JavaScript files, it puts their global variables in the same place. Consider the following:  
`<script src="one.js"></script>`  
`<script src="two.js"></script>`

If `one.js` defines a variable, that same variable will appear in `two.js`. You can use it in `two.js` as if it were defined there. Just remember that you have to load them in the correct order. This applies to function definitions too, and allows us to split code into multiple files like this:  
`<script src="brick.js"></script>`  
`<script src="extra.js"></script>`  
`<script src="header.js"></script>`  
`<script src="body.js"></script>`  
`<script src="footer.js"></script>`

In this example, we load Brick, and then a file called `extra.js` which presumably contains some definitions shared between the later files, and then we load the header, body and footer of the page through their individual files. Check out example 1 to see this in action.

# Isn't this just ReactJS/AngularJS?
Almost. You could even use this library to create single page webapps. And that's the takeaway from all of this. When we ditch HTML and CSS and exclusively use JS, and create a wrapper library to make node creation easier, we end up with modern web dev. It's easier than dealing with HTML and CSS manually, and you get all of the power JavaScript provides.
