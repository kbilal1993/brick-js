# Pages, an optional extension to BrickJS
Pages is a library made to make single-page webapps easier. It has it's own built-in structure, with a title page on the top and sidebar with various links to pages. The Pages library is exposed in the form of a class called WebApp, which contains everything you need.

Pages itself is not required to use Brick JS, and is probably not needed for every case. If you find yourself making a tabbed web page you might want to consider it.

# Usage
First, add this to your HTML file:

`<script src='brick-pages.js'></script>`

And then in the JS file, add this:

```
var webpage = new WebApp(); // Instantiate the webapp
webapp.title = "My fancy single-page webapp!"; // Give it a title. This appears at the top and in the title of the web page
webapp.pages = pages; // We'll get to this later
webpage.apply(); // Puts it in the web page
```

## Pages
webapp.pages is expected to be a list of objects describing individual pages. Each object should have a title of type `string`, which will be used to identify it, and a contents of type `() => DOM node`, which will contain the web page. This seems complicated, but can be easily split up to be easier to work with. The single page webapp example demonstrates this by defining the functions individually, and then making a `pages` object which uses them.

Note that the first page is the one that gets loaded initially, and the pages appear in the order they're found.

## Style hooks
Pages has a nice, minimalist default style. You can override this by using the style hooks provided with the WebApp object. Each hook is a function that takes a value and returns nothing, and that value it takes represents the DOM node to be restyled. For example, to change the size of the titlebar you can do this:

```
function my_titlebar_change(t) {`
  t.style.fontSize = "large";
  t.style.textAlign = "center";
}
```

```
var webpage = new WebApp();
webapp.title = "Test";
webapp.pages = pages;
webapp.style_titlebar = my_titlebar_change;
webapp.apply();
```

## Other hooks
Sometimes you might want to restrict a user's access to a page. In that case you can use `hook_load_page`, a function which is called whenever a page is accessed. If that function returns false, the page doesn't change. You could use this to restrict a user's access to a page while something else is going on. Node that this is all clientside JS, so don't expect this to keep curious web devs out. If your platform's security needs to depend on this, maybe reconsider what you're trying to achieve.

# Example
The Single Page Webapp example uses Pages. (originally it didn't but now that Pages exists there's no point letting it go unused) 
