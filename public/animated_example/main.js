// Create the canvas to draw the background image on
// The canvas itself isn't added to the HTML, but we take the drawing it generates and use that as the background
var [c, ctx] = canvas();
c.width = 100;
c.height = 100;


function draw_bg_image() {
    ctx.beginPath();
    var gradient = ctx.createLinearGradient(0, 100, 100, 0);
    gradient.addColorStop(0, "blue");
    gradient.addColorStop(1, "purple");
    ctx.strokeStyle = gradient;
    ctx.lineWidth = 10;
    ctx.arc(50, 50, 30, 0, 2 * Math.PI);
    ctx.stroke();
}

draw_bg_image();

// Create the general web page layout
var contents = div_center(); // special div that centers everything
var centre_box = div_center();

centre_box.style.width = "70%";
centre_box.style.height = "70%";
centre_box.style.backgroundColor = "white";
centre_box.style.border = "2px solid black";
centre_box.style.boxShadow = "7px 7px 2px #000000aa";

add(contents, centre_box);
add(root, contents);

// Add content to the page itself
var inner_title = h1("WELCOME TO MY WEB PAGE");
var inner_text = p("SORRY IF IT MAKES YOU DIZZY");
var cat_box = div();
function new_cat() {
    var cat_gif = img('animated_example/cat.gif');
    // btw I don't own the cat gif and don't know the copyright. If the owner asks I'll use something else
    cat_gif.style.height= 200;
    cat_gif.style.width= 200;
    return cat_gif;
}

add(cat_box, new_cat());
add(cat_box, new_cat());
add(cat_box, new_cat());
add(centre_box, inner_title);
add(centre_box, inner_text);
add(centre_box, cat_box);

// The background effect doesn't work properly on Chrome, so I'm disabling it for that browser. If you want to throw up you'll have to use Firefox
if (window.chrome) {
  	inner_text.textContent = "Sorry but the fancy background doesn't work properly in Chrome because it pauses the effect when the mouse isn't moving. Try using Firefox."
	root.style.background = `url(${c.toDataURL()})`;
} else {
// Setting the background and making it move
// I wonder if this uses more resources than CSS's built-in animations system
	root.style.background = `url(${c.toDataURL()})`;
	setInterval(move_bg, 1000 / 60); // 60 frames per second

	var move_bg_amount= 0;

// Returns a rectangular coordinate from a polar one
	function polar_to_rectangular(r, theta) {
		return [r * Math.cos(theta), r * Math.sin(theta)];
	}

// Moves the background. Changing the first argument of this function will increase the spinniness
	function move_bg() {
		var coords = polar_to_rectangular(300, move_bg_amount / 100);
    	root.style.backgroundPosition = `${coords[0]}px ${coords[1]}px`;
    	move_bg_amount++;
	}
}
