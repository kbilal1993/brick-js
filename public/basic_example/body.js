var dummy_contents = [
    {"title": "Not a real page btw. These links go nowhere.", "link": "/"},
    {"title": "JavaScript basics", "link": "/"},
    {"title": "Map, Reduce and Filter", "link": "/"},
    {"title": "JavaScript and data structures", "link": "/"},
    {"title": "AJAX and fetch()", "link": "/"},
    {"title": "WebSockets and bidirectional communication", "link": "/"},
    {"title": "DOM manipulation", "link": "/"},
];

function make_article(contents) {
    var article_header = h2(contents.title);
    var article = make_box([article_header]);
    article.onclick = () => location.href = contents.link;
    article.style.cursor = 'pointer';
    article_header.style.margin = 10;
    article.style.width = "fit-content";
    return article;
}

var articles = dummy_contents.map(make_article);

var body_div = div();
articles.forEach(x => add(body_div, x));

add(root, body_div);
