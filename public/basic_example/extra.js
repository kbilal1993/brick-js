// Defining this specifically for this example
// I want to encase everything in nice, drop-shadowed boxes
// This is a wrapper function to make this easier
function make_box(contents) {
    var box = div();
    contents.forEach(x => add(box, x));

    // Styling the box
    box.style.display = "flex";
    box.style.backgroundColor = "lavender";
    box.style.boxShadow = "3px 3px";
    box.style.border = "1px solid";
    box.style.margin = 10;
    return box;
}
