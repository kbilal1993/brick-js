function make_header() {
    var header_title = h1("JavaScript Reference"); // Title to be inserted into the header
    var header_navbar = new_element('nav'); // Nav element acts like a div but lets the browser know it's a navbar
    var header_navbar_contents = [ // the stuff I'll put into the header
        a("Home", "/"),
        a("Guides", "/"),
        a("Reference", "/"),
        a("Troubleshooting", "/")
    ];
    // Add the elements together
    header_navbar_contents.forEach(x => add(header_navbar, x));
    var header = make_box([header_title, header_navbar]); // div containing the header

    // Style the header
    header.style.alignItems = "baseline";
    header.style.justifyContent = "center";
    // Style the title
    header_title.style.margin = 10;
    // Style the links
    header_navbar_contents.forEach(x => {
        x.style.margin = 2;
    });
    return header;
}

// remove for final version
add(root, make_header());
