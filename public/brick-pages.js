// Brick JS plugin to create a single page webapp with tabs on the side

// Make sure to check that Brick JS is loaded
if (!typeof(brickjs)) {
  	window.stop();
  	console.error("You need to load Brick JS first");
}

// class for single page webapp
// Has:
//   title
//   sidebar
//   page list
class WebApp {
	title = "";
	pages = [];

	div_page = undefined;
	div_panel = undefined;
	div_sidebar = undefined;

	style_titlebar = x => { // redefinable hook to style titlebar
		x.style.textAlign = "center";
	};
	style_sidebar_button = x => { // redefinable hook to style button
		x.style.backgroundColor = "lavender";
    	x.style.border = "1px solid black";
    	x.style.boxShadow = "1px 1px 2px";
    	x.style.margin = "3px 10px";
    	x.style.padding = "7px";
	}
	style_sidebar_button_active = x => { // redefinable hook to style pressed button
		x.style.backgroundColor = "lavender";
    	x.style.border = "1px solid black";
    	x.style.boxShadow = "inset 1px 1px 1px";
    	x.style.margin = "3px 10px";
    	x.style.padding = "7px";
	}
    hook_load_page = () => true;
  	constructor () {};

	// Load a page. p = string name of page
  	load_page(p) {
  		if (this.hook_load_page(p)) {
  			var page = this.pages.find(x => x.title == p);
  			var page_button = this.div_sidebar.children[`sidebar_${p}`];

  			// Remove previous contents of panel
  			if (this.div_panel.children.length == 1) {
  				remove(this.div_panel.children[0]);
  			}

  			// Add new contents
  			add(this.div_panel, page.contents());

  			// Set all buttons inactive
  			for (var x = 0; x < this.div_sidebar.children.length; x++) {
  				this.style_sidebar_button(this.div_sidebar.children[x]);
  			}

  			// Set the current button active
  			this.style_sidebar_button_active(page_button);

  			// Set the web page title
  			document.title = `${this.title} : ${p}`;
  		}
  	}

	generate() {
		if (!this.pages || !this.title) {
			console.error("You need to give the webapp some pages and/or a title!");
		}
		// Generate main page
		var temp = div();

		var _main_page = div(); // overall full-page div
		_main_page.style.display = "flex";
		_main_page.style.justifyContent = "center";

		var _title = h1(this.title); 
		this.style_titlebar(_title);

		var _sidebar = div(); // sidebar div
		_sidebar.style.display = "flex";
		_sidebar.style.flexDirection = "column";
		_sidebar.style.flexShrink = 0;
		_sidebar.style.flexGrow = 0;

		var _panel = div(); // main page panel
		_panel.style.maxWidth = "50em";
		_panel.style.width = "100%";
		_panel.style.padding = "0px 5px";

		// Generate side panel
        this.pages.map(p => {
            var _button = new_text_element('span', p.title);
            _button.onclick = () => this.load_page(p.title);
            _button.id = `sidebar_${p.title}`;
    	    _button.style.cursor = 'pointer';
            this.style_sidebar_button(_button);
            add(_sidebar, _button);
        });

        // Put everything together
        add(temp, _title);
        add(temp, _main_page);
        add(_main_page, _sidebar);
        add(_main_page, _panel);

        // set variables
        this.div_page = temp;
        this.div_sidebar = _sidebar;
        this.div_panel = _panel;
	};

	apply() {
		this.generate();

		// Add to DOM
		add(root, this.div_page);

		// Open the first page
        this.load_page(this.pages[0].title);
	}
}

