// Core definitions
/**
* Wrapper function for creating a DOM node of the given tag
* @param {string} tag - The HTML tag to be used for the node
*/
function new_element(tag) { // Define a base element
    var element = document.createElement(tag);
    return element;
}

/**
* Wrapper to create a new text element, that automatically populates it with given text.
* @param {string} tag - The HTML tag for the DOM node
* @param {string} text - The text to be inside the DOM node
*/
function new_text_element(tag, text) { // New text element. Something like p or h1
    var element = new_element(tag);
    var node = new_text(text);
    element.appendChild(node);
    return element;
}

/**
* Wrapper to create an input element. This creates a label element, with an input element of the given type inside it.
* It also defines .input as a shortcut for the input DOM node inside the label one.
* @param {string} type - Type of input. the one you'd add for type='' in HTML
* @param {string} text - Text for the label.
*/
function new_input_element(type, text) { // New input element, wrapped in a label for convenience
    var temp = new_element('input');
    var label = new_text_element('label', text);
    temp.type = type;
    add(label, temp);
    label.input = temp; // Access child input node from here
    return label;
}

/**
* Wrapper to create a text node and return it
* @param {string} str - String to be inside the text node
*/
function new_text(str) { // Turn string into text node
    var temp = document.createTextNode(str);
    return temp;
}

/**
* Wrapper to add one DOM node to the other as a child
* Literally a wrapper for appendChild.
* @param {HTMLElement} tag - A DOM node to act as a parent
* @param {HTMLElement} child - A DOM node to be appended to the parent
*/
// Node manipulation
function add(tag, child) { // Add the second tag to the first as a child
    tag.appendChild(child);
}

/**
* Wrapper to remove a DOM node. Remember that just setting a variable that points to one as null doesn't remove it.
* If used on a DOM node with children, recursively removes those too.
* @param {HTMLElement} node - DOM node to remove
*/
function remove(node) { // Delete the given tag. It's good practice to do this explicitly
    if (node.children.length == 0) {
      node.remove();
    } else {
        for (var x = 0; x < node.children.length; x++) {
            remove(node.children[x]);
        }
        remove(node);
    }
}

// Tag definitions
/**
* Create and return a <div></div> element
*/
function div() { // div. For structuring the web page. Used as a container
    var temp = new_element('div');
    return temp;
}

/**
* Create and return a <span></span> element
*/
function span() { // Span. For structuring the web page
    var temp = new_element('span');
    return temp;
}

/**
* Create and return a <div></div> element
* Automatically sets it as a flexbox, and tries to center the children in a columnar list
* For when you want something in the middle of the screen, or the middle of an element
*/
function div_center() { // Flexbox which centres everything and stacks them vertically
    var temp = div();
    temp.style.display = "flex";
    temp.style.flexDirection = "column";
    temp.style.justifyContent = "center";
    temp.style.alignItems = "center";
    temp.style.width = "100%";
    temp.style.height= "99%"; // or will make the page scroll awkwardly
    return temp;
}

/**
* Alternate spelling for div_center
*/
var div_centre = div_center; // for the correct spelling

/**
* Create and return an ordered list element
* @params {any[]} lst - Either a list of HTMLElements, which are added as-is, or a list of strings, which are automatically wrapped in text elements and added
*/
function ol(lst) { // take list of nodes, return ordered list
    var temp = new_element('ol');
    for (var x = 0; x < lst.length; x++) {
        var t = new_element('li');
        var input = lst[x];
        if (!input.getRootNode) {
            input = new_text(input);
        }
        add(t, input);
        add(temp, t);
    }
    return temp;
}

/**
* Create and return an unordered list element
* @params {any[]} lst - Either a list of HTMLElements, which are added as-is, or a list of strings, which are automatically wrapped in text elements and added
*/
function ul(lst) { // take list of nodes, return unordered list
    var temp = new_element('ul');
    for (var x = 0; x < lst.length; x++) {
        var t = new_element('li');
        var input = lst[x];
        if (!input.getRootNode) {
            input = new_text(input);
        }
        add(t, input);
        add(temp, t);
    }
    return temp;
}

/**
* Create and return a table element
* @params {any[]} llst - List of lists containing table data. Can be HTMLElements, which are added as-is, or strings, which are automatically wrapped in text elements and added. The overall list represents the number of rows of the table, and the internal lists represent the contents of each row
*/
function table(llst) { // table. Takes list of lists of nodes, returns a HTML table.
    var temp = new_element('table');
    for (var y = 0; y < llst.length; y++) {
        var yy = new_element('tr');
        for (var x = 0; x < llst[y].length; x++) {
            var xx = new_element('td');
            var input = llst[y][x];
            if (!input.getRootNode) { // if not given a node, wrap given info in a text node
                input = new_text(input);
            }
            add(xx, input);
            add(yy, xx);
        }
        add(temp, yy);
    }
    return temp;
}

// Simple tags
/**
* Returns a <p>text</p>
* @params {string} text - The text for the element
*/
function p(text) { // Paragraph
    var temp = new_text_element('p', text);
    return temp;
}

/**
* Returns an <a href='href'>text</a>
* @params {string} text - The text for the hyperlink itself
* @params {string} href - Where the hyperlink should link
*/
function a(text, href) { // Link. Remember to add the URL you're linking to
    var temp = new_text_element('a', text);
    temp.href = href;
    return temp;
}

/**
* Returns a <h1>text</h1>
* @params {string} text - The text inside the header
*/
function h1(text) { // Header 1
    var temp = new_text_element('h1', text);
    return temp;
}

/**
* Returns a <h2>text</h2>
* @params {string} text - The text inside the header
*/
function h2(text) { // Header 1
    var temp = new_text_element('h2', text);
    return temp;
}

/**
* Returns a <h3>text</h3>
* @params {string} text - The text inside the header
*/
function h3(text) { // Header 1
    var temp = new_text_element('h3', text);
    return temp;
}

/**
* Returns a <br>, or break.
*/
function br() { // Line break
    var temp = new_element('br');
    return temp;
}

/**
* Returns a <hr>, or horizontal line
*/
function hr() { // Horizontal rule
    var temp = new_element('hr');
    return temp;
}

/**
* Returns a textbox wrapped in a label
* Also binds .value to the value of the text in the textbox
* @params {string} text - The text in the label
*/
function textbox(text) { // A textbox, wrapped in a label
    var temp = new_input_element('text', text);
    temp.value = () => temp.input.value; // hoisting this up for convenience
    return temp;
}

/**
* Returns a numberbox wrapped in a label
* Like a textbox but only takes numbers
* Also binds .value to the value inside the numberbox
* @params {string} text - the text in the label
*/
function numberbox(text) { // A textbox that only takes numbers. Comes wrapped in a label
    var temp = new_input_element('number', text);
    temp.value = () => temp.input.value; // hoisting this up for convenience
    return temp;
}

/**
* Returns a checkbox wrapped in a label
* Also binds .value to whether the box is checked or not
* @params {string} text - The text in the label
*/
function checkbox(text) { // A checkbox, wrapped in a label
    var temp = new_input_element('checkbox', text);
    temp.value = () => temp.input.checked; // hoisting this up for convenience
    return temp;
}

/**
* Returns a clickable button
* Set it to do something with .onclick
* @params {string} text - The text on the button
*/
function button(text) { // A clickable button. Calls onclick when clicked
    var temp = new_element('input');
    temp.value = text;
    temp.type = "button";
    return temp;
}

/**
* Returns a list of radio buttons wrapped in labels
* The first value in the list is automatically selected
* Binds .value to the element that's currently selected
* @params {String[]} lst - List of strings to use as the labels
*/
function radio(lst) { // Takes a list of strings. Returns a div containing radio elements
    var temp = div();
    for (var x = 0; x < lst.length; x++) {
        var r = new_input_element('radio', lst[x]);
        if (x == 0) {r.input.checked = "checked";};
        r.value = () => r.input.checked;
        add(temp, r);
    }
    temp.value = () => {
        var t = temp.children.filter(x => x.value());
        if (t.length == 0) {
            return false;
        } else {
            return t[0];
        }
    };
    return temp;
}

/**
* Returns a canvas element, automatically set to 2d context.
* @returns A list where the first value is the canvas and the second is it's context.
*/
function canvas() { // a Canvas. Used for generated graphics and such. Remember to set width and height manually. define with var [c, ctx] = canvas()
    var c = new_element('canvas');
    var ctx = c.getContext('2d');
    return [c, ctx];
}

/**
* Returns an image element
* Remember to resize it to fit
* @param {string} url - The URL for the image
*/
function img(url) { // An image. Takes the URL. Remember to set width and height manually
    var temp = new_element('img');
    temp.src = url;
    return temp;
}

// Variables for simplicity
/**
* Bound to document.documentElement
* The root of the document. Where the <html></html> tag usually is
*/
const root = document.documentElement; // The root of the document. Why not the <body> tag? Because it'll throw an error if it doesn't find <body> in the HTML

// Make the page look better on Chrome browsers. Specifically we don't use the body because it doesn't load in time, and Chrome-likes still render it as blank space on the bottom of the page.
window.addEventListener('load', () => remove(document.body));

// Set variable so we know Brick JS is loaded
const brickjs = true;
