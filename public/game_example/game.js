// variables
var game_width = 640;
var game_height = 480;

// Setting up the game window
var [c, ctx] = canvas();

c.width = game_width;
c.height = game_height;
c.style.width = game_width;
c.style.height = game_height;
c.style.border = "1px solid black";
c.style.boxShadow = "2px";

var game_container = div_center();

var instructions = p("Use the arrow keys to move the guy");

add(game_container, c);
add(game_container, instructions);
add(root, game_container);

// Game logic
var guy = {
    "x": game_width / 2,
    "y": game_height / 2,
}

function draw_guy(g, colour) {
    ctx.beginPath();
    ctx.arc(g.x, g.y, 10, 0, 2 * Math.PI);
    ctx.fillStyle = colour;
    ctx.fill();
}

function clear() {
    ctx.beginPath();
    ctx.rect(0, 0, game_width, game_height);
    ctx.fillStyle = "white";
    ctx.fill();
}

document.onkeydown = key => {
    draw_guy(guy, "gray");
    switch (key.keyCode) {
    case 38: // up
        if (guy.y > 10) {guy.y = guy.y - 10;};
        break;
    case 40: // down
        if (guy.y < game_height - 10) {guy.y = guy.y + 10;};
        break;
    case 37: // left
        if (guy.x > 10) {guy.x = guy.x - 10;};
        break;
    case 39: // right
        if (guy.x < game_width - 10) {guy.x = guy.x + 10;};
        break;
    }
    draw_guy(guy, "black");
};

draw_guy(guy, "black");
