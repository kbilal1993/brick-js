// Defining base structure
var outer_container = div_center();
var inner_container = div_center();

inner_container.width = "70%";

// Adding input elements
var string_input = textbox("");
var string_output = p("---");

// Making them work. oninput = whenever text is added or removed
string_input.input.oninput = () => {
    if (string_input.value() == "") {
        string_output.innerHTML = "...";
    } else {
        string_output.innerHTML = string_input.value();
    }
};

// Adding everything together
add(inner_container, string_input);
add(inner_container, string_output);

add(outer_container, inner_container);

add(root, outer_container);
