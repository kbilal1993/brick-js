// Pages are defined here

// Pages are defined here. key = name of the page, value = function that returns the div that gets fed into the page

var pages = [
	{
		"title": "Main",
		"contents": page_main,
	},
	{
		"title": "Addition",
		"contents": page_addition,
	},
	{
		"title": "Subtraction",
		"contents": page_subtraction,
	},
]

// This function returns a page. It wraps it's contents in a <div> DOM node called 'result' and returns it.
function page_main() {
    var page_title = h2("This is the main page");
    var contents = p("Here is some text. Woo it's a single page webapp!");
    var result = div();
    add(result, page_title);
    add(result, contents);
    return result;
}

function page_addition() {
    var result = div();
    var page_title = h2("Addition");
    var in1 = numberbox("");
    var in2 = numberbox("");
    var result_box = p("Result:");
    add(result, page_title);
    add(result, in1);
    add(result, in2);
    add(result, result_box);

    function add_values() {
        if (in1.value() && in2.value()) { // If a user has typed something
            result_box.innerHTML = "Result: " + (parseFloat(in1.value()) + parseFloat(in2.value()));
        }
    }

    // oninput = when the contents of the textbox changes
    in1.input.oninput = () => add_values();
    in2.input.oninput = () => add_values();
    return result;
}


function page_subtraction() {
    var result = div();
    var page_title = h2("Subtraction");
    var in1 = numberbox("");
    var in2 = numberbox("");
    var result_box = p("Result:");
    add(result, page_title);
    add(result, in1);
    add(result, in2);
    add(result, result_box);

    function add_values() {
        if (in1.value() && in2.value()) {
            result_box.innerHTML = "Result: " + (parseFloat(in1.value()) - parseFloat(in2.value()));
        }
    }

    in1.input.oninput = () => add_values();
    in2.input.oninput = () => add_values();
    return result;
}


// Setting the web page up
var webpage = new WebApp();
webpage.title = "My Single-page Webapp";
webpage.pages = pages;
webpage.apply();
