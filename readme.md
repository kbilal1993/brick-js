# Brick JS, the clientside JavaScript website development library
# Why?
One day I saw a post by someone who was proud about not using JavaScript to develop websites and thought, "What if I exclusively used JavaScript to develop websites?".
# Trying it out
Clone this repo and open the HTML example files in your browser. Alternatively you can use the following links:  
[Basic example](https://kbilal1993.gitlab.io/brick-js-legacy/1_basic_example.html)  
[Game example](https://kbilal1993.gitlab.io/brick-js-legacy/2_game_example.html)  
[Animated example](https://kbilal1993.gitlab.io/brick-js-legacy/3_animated_example.html)  
[Input example](https://kbilal1993.gitlab.io/brick-js-legacy/4_input_test.html)  
[Single Page Webapp example](https://kbilal1993.gitlab.io/brick-js-legacy/5_single_page_webapp_example.html)  
# Getting started
Instructions can be found in `introduction.md`.
# Improvements and development direction
Right now I want to add essential features to `brick.js`. Things you'd reasonably want in every website. Once I'm happy with everything I'll tackle the rest of the list:
- Add more documentation
- Make a serious project using this
- Improve Brick JS Pages
- Create more extensions like Pages
- Maybe look into extra functions for Bootstrap integration
