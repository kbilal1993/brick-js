# Variables
## root
Bound to document.documentElement. This is the root of the web page. Using add(root, x) adds DOM node x to the root of the page. Elements have to be added to the root either directly or through other elements to appear in the page.
# Core functions
## new_element(tag)
Returns a DOM node with the given tag. Wrapper for document.createElement(tag);  
`var x = new_element('p');`  
`->  <p>`  
## new_text_element(tag, text)
Same as above, but also creates a text node containing the given text, puts that in the main node and returns that. This lets you add text to your DOM nodes easily.  
`var x = new_text_element('p', 'Hello');`  
`->  <p>Hello</p>`  
## new_input_element(type, text)
Returns a DOM node representing a `<label>`, containing another node for an `<input>`. The type parameter determines the type of the `<input>` tag, and the text parameter is the textual contents of the label.  
The returned node has an extra feature. `node.input` points towards the `<input>` node.  
`var x = new_input_element('text', 'Enter your name');`  
`->  <label>Enter your name<input type='text'></label>`  
## new_text(string)
Returns a DOM text node containing the given string. Remember that raw text inside tags is technically inside a text node, so if you want plain text in your HTML you're probably looking for this.  
`var x = new_text('hello');`  
`->  hello`  
# Node manipulation
## add(parent, child)
Takes two DOM nodes, a parent and a child. Appends the child node to the parent. You can do this with multiple child nodes one after another to add more nodes. You'll generally use this to add nodes to a `<div>`, or to add to the root of the page. Wrapper for parent.appendChild(child);  
`var d = div();`  
`var text = p('hi');`  
`add(d, text);`  
`->  <div><p>hi</p></div>`  
## remove(node)
Recursively removes a DOM node and all of it's children. Remember that removing a variable that points to a DOM node doesn't get rid of it, so use this. Wrapper for node.remove();
# Simple tags
## p(text)
Returns a DOM node with tag `p` and contents `text`.  
`var x = p('hello');`  
`->  <p>hello</p>`
## a(text, url)
Returns a DOM node with tag `a` and contents `text`. The URL parameter automatically sets the URL for convenience.  
`var x = a('hello', 'http://example.com');`  
`->  <a href='http://example.com'>hello</a>`
## h1(text), h2(text), h3(text)
Returns a DOM node with the respective tag. The text parameter becomes the text inside the tag.  
`var x = h1('hello');`  
`->  <h1>hello</h1>`
## br()
Returns a `<br>`. Used as a line break.
## hr()
Returns a `<hr>`. Draws a horizontal line. Used as a separator.
## img(url)
Returns a node representing an image. Remember to set node.style.width and node.style.height to specify the size of the image. The URL parameter is the URL of the image.  
`var x = img('http://example.com/image.png');`  
`->  <img src='http://example.com/image.png'></p>`
# Structure tags
## div()
Returns a DOM node representing a `<div>`. You can set the ID or class later with `node.id` and `node.class`
## div_center() and div_centre()
Same as div(), but with some extra changes. Automatically makes it a flexbox, aligns everything towards the centre, vertically stacks contents and sets the size to be as large as possible. If you vertically centre things a lot you'll like this.
## span()
Returns a DOM node representing a `<span>`. It's like a `<div>` but doesn't shift things around as much.

# Lists and tables
## ol(lst)
Takes a list of either DOM nodes or strings. Returns a DOM node representing an ordered list, with the contents being the lst parameter. Strings are automatically wrapped in text nodes, and each element is automatically wrapped in `<li>` nodes.  
`var my_list = ol(["One", "Two", "Three"]);`  
`-> <ol>`  
`     <li>One</li>`  
`     <li>Two</li>`  
`     <li>Three</li>`  
`   </ol>`
## ul(lst)
Same as ol(lst), but returns an unordered list. The list uses bullet points instead of numbers.
## table(llst)
Takes a list of lists of DOM nodes or strings, returns a DOM node representing a table. `<tr>` and `<td>` are used where they need to be. Strings are automatically wrapped in text nodes.  
`var my_table = table([["X", "X ^ 2"], [2, 4], [3, 9], [5, 25]]);`  
`->  <table>`  
`      <tr>`  
`        <td>X</td><td>X ^ 2</td>`  
`      </tr>`  
`      <tr>`  
`        <td>2</td><td>4</td>`  
`      </tr>`  
`      <tr>`  
`        <td>3</td><td>9</td>`  
`      </tr>`  
`      <tr>`  
`        <td>5</td><td>25</td>`  
`      </tr>`  
`    </table>`

# Input nodes
## textbox(text)
Takes a string, returns a label node with embedded input node. The input node is set to `text`. The returned node has an extra function value(), which returns the contents of the textbox  
`var x = textbox('Name:');`  
`->  <label>Name:<input type='text'></label>`  
`// type 'Foo' in the textbox`  
`x.value();`  
`->  "Foo"`
## numberbox(text)
Same as textbox(text), but only allows numbers.  
`var x = numberbox('Name:');`  
`->  <label>Name:<input type='number'></label>`  
`// type '5' in the textbox`  
`x.value();`  
`->  5`
## checkbox(text)
Same as textbox(text), but returns a checkbox. The function value() is bound to .checked, so it still returns the value of the checkbox.  
`var x = checkbox('Name:');`  
`->  <label>Name:<input type='checkbox'></label>`  
`// Tick the checkbox`  
`x.value();`  
`->  true`
## radio(lst)
Takes a list of strings, returns a `<div>` node containing `<label>` and `<input>` pairs for every value in lst. Adds .value() to each radio button to check whether it is checked or not. Adds .value() to the div itself, which returns the specific checked node (or false if none are checked).  
`var x = radio(["One", "Two", "Three"]);`  
`->  <div>`  
`      <label>One<input type='radio' checked></label>`  
`      <label>Two<input type='radio'></label>`  
`      <label>Three<input type='radio'></label>`  
`    </div>`  
`// Click the button next to Two`  
`x.value();`  
`->  <label>Two<input type='radio checked></label>`
# Other
## canvas()
Returns a list containing the `<canvas>` DOM node and the 2D context for the canvas. You can do this to define both at the same time:   
`var [c, ctx] = canvas();`
